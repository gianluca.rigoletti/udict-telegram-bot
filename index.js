const TeleBot = require("telebot")
const fetch = require("node-fetch")
const outdent = require("outdent")
require("dotenv").config()
const url = "http://api.urbandictionary.com/v0/define?term="

const bot = new TeleBot({
  token: process.env.TOKEN // Required. Telegram Bot API token.
})

bot.on(/^\/search (.+)$/, (msg, props) => {
  const text = props.match[1]
  fetch(url + text)
    .then(response => response.json())
    .then(async response => {
      const tagsResponse = response.tags.join(" ")
      await bot.sendMessage(msg.from.id, `Tags: **${tagsResponse}**`, {
        parseMode: "Markdown"
      })
      response.list.slice(0, 3).map(async el => {
        const reply = outdent`
        *word*: ${el.word}
        *definition*: 
        ${el.definition}
        *example*: 
        _${el.example}_
        ${el.thumbs_up} 👍 ${el.thumbs_down} 👎
        [open in browser](${el.permalink})
        `
        await bot.sendMessage(msg.from.id, reply, {
          parseMode: "Markdown",
          webPreview: false
        })
      })
    })
})

bot.on("inlineQuery", msg => {
  const query = msg.query
  const answers = bot.answerList(msg.id, { cacheTime: 60 })
  console.log(query)
  fetch(url + query)
    .then(response => response.json())
    .then(response => {
      response.list.slice(0, 2).map(el => {
        console.log(el)
        const definition = el.definition.replace(/\r?\n|\r|\"/g, " ")
        answers.addArticle({
          id: el.permalink,
          title: el.word,
          description: definition,
          parse_mode: "Markdown",
          disable_web_page_preview: true,
          message_text: outdent`
          *word*: ${el.word}
          *definition*: 
          ${el.definition}
          *example*: 
          _${el.example}_
          ${el.thumbs_up} 👍 ${el.thumbs_down} 👎
          [open in browser](${el.permalink})
          `
        })
      })
    })
    .then(() => {
      bot.answerQuery(answers)
    })
})

bot.start()
